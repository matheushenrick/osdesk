﻿# **************    BUILD E ASSINATURA DE APK IONIC para ANDROID    ********************** #
# Criador: Matheus Henrique                                                                #
# Data: 04/12/2019                                                                         #
# Script dinamico, preencher as variaveis $pasta, $app, $nomeCertificado, $caminhoCertific #
# ado, $aliasCertificado, $passwordCertificado e $caminhoBuild                             #
# Pré-Requisitos:                                                                          #
# Possuir configurado a variavl de Ambiente JAVA_HOME para utilizacao do jarsigner         #
# Possuir Android Studio configurado para utilizacao do zipalign                           #
# Possuir Ionic e Cordova configurados                                                     #
# **************    BUILD E ASSINATURA DE APK IONIC para ANDROID    ********************** #

# ****** DECLARACAO DE VARIAVEIS ****** #
# pasta do projeto
$pasta = "osdesk";
# Nome do aplicativo a ser gerado
$app = "osdesk.apk";
# Nome do certificado
$nomeCertificado = "my-release-key.keystore";
#caminho do certificado
$caminhoCertificado = "C:\Users\matheusha\Documents\$nomeCertificado";
# Alias do certificado
$aliasCertificado = "audPro_Auditoria_De_Produtos";
# Password do certificado
$passwordCertificado = "Mh954945";
#caminho do build
$caminhoBuild = "D:\Ionic\$pasta\platforms\android\app\build\outputs\apk\release";
# Atualizar as libs do NPM? 0 para NAO e 1 para SIM
$npmUpdate = 1;
# Caso queira mensagem pos success build
$successAssing = 0;


try {
    # Acessa o diretorio para iniciar o processo
    Set-Location $caminhoBuild;

    # Atualizacao das libs NPM
    if($npmUpdate -eq 1){
        npm update
    }

    # Sempre vai gerar uma nova build do app
    ionic cordova build android --release --prod

    # Verifica se ja existe o certificado, caso nao exista cria uma copia da pasta documentos
    $caminho = Get-ChildItem -Path $caminhoBuild;

    #controle de certificado
    $certificado = 0;
    foreach ($item in $caminho) {

        # Verifica se existe o certificado
        if ($item.Name -eq $nomeCertificado) {

            $certificado = 1;
        }
        
        # Verifica se ja existe o arquivo final, caso exista, remove
        if ($item.Name -eq $app) {
    
            Remove-Item $item;
        }
    }

    # Caso tenha o certificado no local
    if ($certificado -eq 1) {
        # Assina o app com o certificado
        jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $nomeCertificado app-release-unsigned.apk $aliasCertificado -storepass $passwordCertificado
    }
    # Caso nao tenha o certificado
    else {
        Copy-Item $caminhoCertificado -Destination $caminhoBuild;
        # Assina o app com o certificado
        jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $nomeCertificado app-release-unsigned.apk $aliasCertificado -storepass $passwordCertificado
    }

    # Alinha o aplicativo e gera um novo com o nome do app
    zipalign -v 4 app-release-unsigned.apk $app

    if($successAssing -eq 1){
        Read-Host -Prompt "Build e Assinatura do APK realizado com sucesso. Pressione alguma tecla para sair";
    }
}
catch {
    Write-Error $_Exeption.ToString();
    Read-Host -Prompt "Ops! Algo de errado não está certo. Pressione alguma tecla para sair";
}