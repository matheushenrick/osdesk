import { Component } from '@angular/core';
import { NotificacaoService } from '../service/notifica/notificacao/notificacao.service';
import { OrdermservicoService } from '../ordemservico/ordermservico.service';
import { AplicacaoService } from '../service/aplicacao/aplicacao.service';

@Component({
	selector: 'app-home',
	templateUrl: 'home.page.html',
	styleUrls: ['home.page.scss'],
})

export class HomePage {
	public notificacao: any = {}
	public osHome : any = {};

	public show = {Recentes : false, Andamento : false, Encerrados : false}

	constructor(private notiSevices : NotificacaoService, private oService : OrdermservicoService, private app : AplicacaoService) { }

	ngOnInit() {
		this.notificacao = this.notiSevices.getAllNotify();
		this.getDadosOs();
	}

	async getDadosOs(){
		let os = await this.oService.getDadosOS().then((response) =>{
			this.osHome = response
		})
		console.log(this.osHome);
		return os;
	}

	verTodas(page){
		this.app.navegar(page);
	}

}