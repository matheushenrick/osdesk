import { Component } from '@angular/core';

import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { WeatherService } from './service/weather/weather.service';
import { NotificacaoService } from './service/notifica/notificacao/notificacao.service';
import { OnesignalService } from './service/notifica/onesignal/onesignal.service';
import { AplicacaoService } from './service/aplicacao/aplicacao.service';

@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html',
	styleUrls: ['app.component.scss']
})
export class AppComponent {
	public appPages = [];

	public clima: any = {};
	public notificacao: any = {}
	public dadosUser : any = {};

	constructor(
		private platform: Platform,
		private splashScreen: SplashScreen,
		private statusBar: StatusBar,
		private weather: WeatherService,
		private notify: NotificacaoService,
		private oneSignal: OnesignalService,
		private app: AplicacaoService,
		private menu: MenuController
	) {
		this.initializeApp();
	}

	async initializeApp() {
		this.preencheMenu();
		this.platform.ready().then(async () => {
			this.assingOneSignal();
			await this.getClima();
			this.notify.getQtdNotify().then(response => { this.notificacao = response });
			this.statusBar.styleBlackTranslucent();
			this.splashScreen.hide();
		});
	}

	async getClima() {
		let clima = await this.weather.getWeather().then((response) => {
			this.clima = response;
		})
		return clima;
	}

	viewNotify() {
		this.app.navegar('list-all-notify');
		this.menu.close();
	}

	/**
	 * Assina o dispositivo a trabalhar com as notificacoes do servico OneSignal
	 */
	assingOneSignal() {
		this.oneSignal.setupPush()
	}

	userInfo(){
		
	}

	async preencheMenu() {
		this.appPages = [
			{
				title: 'Home',
				url: '/home',
				icon: 'home'
			}, {
				title: 'Ordem de Serviços',
				children: [{
					title: 'Ordens de Serviços',
					url: '/os_listar',
					icon: 'flag'
				}, {
					title: 'Nova Ordem de Serviço',
					url: '/os_criar',
					icon: 'add'
				},]
			}, {
				title: 'Serviços',
				children: [{
					title: 'Cadastrar Equipe',
					url: '/list_equipe',
					icon: 'list'
				}, {
					title: 'Cadastrar Serviço',
					url: '/list_servico',
					icon: 'list'
				}, {
					title: 'Cadastrar Problema',
					url: '/list_problema',
					icon: 'list'
				}, {
					title: 'Cadastrar Prioridade',
					url: '/list_prioridade',
					icon: 'list'
				}]
			}
		];
	}
}
