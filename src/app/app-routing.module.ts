import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{ path: '', redirectTo: 'login', pathMatch: 'full' },
	{ path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomePageModule) },
	{ path: 'config', loadChildren: './service/config/config/config.module#ConfigPageModule' },
	{ path: 'os_listar', loadChildren: './ordemservico/listar/listar.module#ListarPageModule' },
	{ path: 'os_criar', loadChildren: './ordemservico/criar/criar.module#CriarPageModule' },
	{ path: 'user_listar', loadChildren: './usuario/listar/listar.module#ListarPageModule' },
	{ path: 'user_cadastrar', loadChildren: './usuario/cadastrar/cadastrar.module#CadastrarPageModule' },
	{ path: 'cad_prod', loadChildren: './estoque/cadastro/produto/cadastro.module#CadastroPageModule' },
	{ path: 'cad_list', loadChildren: './estoque/listar/listar/listar.module#ListarPageModule' },
	{ path: 'cad_tipo', loadChildren: './estoque/cadastro/tipo/tipo.module#TipoPageModule' },
	{ path: 'cad_unid', loadChildren: './estoque/cadastro/unidade/unidade.module#UnidadePageModule' },
	{ path: 'cad_disp', loadChildren: './estoque/cadastro/dispo/dispo.module#DispoPageModule' },
	{ path: 'list_prioridade', loadChildren: './servico/prioridade/listar/listar.module#ListarPageModule' },
	{ path: 'list_problema', loadChildren: './servico/problema/listar/listar.module#ListarPageModule' },
	{ path: 'list_equipe', loadChildren: './servico/equipe/listar/listar.module#ListarPageModule' },
	{ path: 'list_servico', loadChildren: './servico/servico/listar/listar.module#ListarPageModule' },
	{ path: 'login', loadChildren: './login/login/login.module#LoginPageModule' },
	{ path: 'view_notify', loadChildren: './service/notifica/notificacao/notificacao.module#NotificacaoPageModule' },
	{ path: 'list-all-notify', loadChildren: './service/notifica/onesignal/list-all-notify/list-all-notify.module#ListAllNotifyPageModule' },
	{ path: 'os_list_aberto', loadChildren: './ordemservico/listar/aberto/aberto/aberto.module#AbertoPageModule' },
	{ path: 'os_list_andame', loadChildren: './ordemservico/listar/andamento/andamento/andamento.module#AndamentoPageModule' },
	{ path: 'os_list_fechad', loadChildren: './ordemservico/listar/encerrado/encerrado/encerrado.module#EncerradoPageModule' },
	{ path: 'os_list_avalia', loadChildren: './ordemservico/listar/avalia/avaliacao/avaliacao.module#AvaliacaoPageModule' },
  { path: 'os_list_aceitas', loadChildren: './ordemservico/listar/aceita/aceitas/aceitas.module#AceitasPageModule' },  { path: 'acompanhar', loadChildren: './ordemservico/acompanhar/acompanhar/acompanhar.module#AcompanharPageModule' },
  { path: 'finalizar', loadChildren: './ordemservico/finalizar/finalizar/finalizar.module#FinalizarPageModule' }


]
	;

@NgModule({
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
	],
	exports: [RouterModule]
})
export class AppRoutingModule { }