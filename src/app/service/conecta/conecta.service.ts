import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, Platform } from '@ionic/angular';
import { AplicacaoService } from '../aplicacao/aplicacao.service';


//Server de requisicao 
export class ConectaService {
	public server = 'https://api.onservices.com.br/';
	// public server = 'http://servidor.local/';
	public token = '0645ca674a752f9724c6b80c73b0808b';

	constructor(private http: HttpClient, private alertCtrl: AlertController, private app: AplicacaoService, private platform : Platform) {

		if(this.platform.is('cordova')){
			this.server = 'https://api.onservices.com.br/';
		}
	}

	/**
	 * get
	 * Funcao para enviar via GET uma requisicao e espera o retorno de um obj - json.
	 * @param rotina nome da rotina que deve ser enviado os dados
	 * @param op nome da funcao a qual deve receber os dados
	 * @param param parametros a serem enviado
	 * @param emp sigla da filial a qual sera destinada Parametro opcional
	 */
	async get(rotina: string, op = '', param = '', debug = true, emp?) {

		if (emp == undefined) {
			// await this.app.getFilial().then((response) => {
			// 	emp = response;
			// });
			emp = "ADM";
		}

		let user = 'matheusha';

		let url = this.server + '?p=' + rotina + (op != '' ? '&op=' + op : '') + (param != '' ? '&' + param : '') + '&keyToken=' + this.token + '&emp=' + emp + '&user=' + user;
		if (debug)
			console.log(url);

		return this.http.get(url).toPromise();
	}

	/**
	 * Apropriadad para chamadas de integração
	 * @param url url a ser chamada via get
	 */
	async getURL(url, debug = false){
		if (debug)
			console.log(url);
			
		return this.http.get(url).toPromise();
	}

	/**
	 * enviaDados
	 * Funcao para realizar o envio de dados para o servidor de destino, envia atravez do metodo POST
	 * @param emp sigla da filial a qual sera destinada
	 * @param rotina nome da rotina que deve ser enviado os dados
	 * @param op nome da funcao a qual deve receber os dados
	 * @param param parametros a serem enviado -  { campo1: dado1, campo2: dados2 }
	 */
	async enviaDados(rotina: string, op: string, param: any, debug = true, emp?) {
		if (emp == undefined) {
			await this.app.getFilial().then((response) => {
				emp = response;
				emp = "ADM";
			});
		}

		let user = 'matheusha';

		let url = this.server + '?p=' + rotina + '&op=' + op + '&keyToken=' + this.token + '&emp=' + emp + '&user=' + user;

		if (debug) {
			console.warn(url);
			console.log(param);
		}

		let headers = new HttpHeaders({ 'Content-Type': 'application/json' });

		return this.http.post(url, param, { headers: headers }).toPromise();
	}

	async exclui(rotina: string, id: Number, debug = true, emp?) {
		if (emp == undefined) {
			await this.app.getFilial().then((response) => {
				emp = response;
				emp = "ADM";
			});
		}

		let user = 'matheusha';

		let url = this.server + '?p=' + rotina + '&op=exclui' + '&id=' + id + '&keyToken=' + this.token + '&emp=' + emp + '&user=' + user;

		if (debug) {
			console.warn(url);
		}

		return this.http.get(url).toPromise();
	}

	async inclui(rotina: string, param: any, debug = true, emp?) {
		if (emp == undefined) {
			await this.app.getFilial().then((response) => {
				emp = response;
				emp = "ADM";
			});
		}

		let user = 'matheusha';

		let url = this.server + '?p=' + rotina + '&op=inclui' + '&keyToken=' + this.token + '&emp=' + emp + '&user=' + user;

		if (debug) {
			console.warn(url);
			console.log(param);
		}

		let headers = new HttpHeaders({ 'Content-Type': 'application/json' });

		// return this.http.post(url, param, { headers: headers }).toPromise();
		return this.http.post(url, param, { headers: headers }).toPromise();
	}

	async serverInfo() {
		let server = await this.get('server', 'returnServer').then((response) => {
			// console.log(response);
			return response;
		}).catch((response) => {
			this.errorHttp(response);
		})

		return server;
	}

	/**
	 * Informa erro de requisição ao usuario
	 * Cria um alert para visualização do erro de requisição
	 * @param erro Erro de requisição
	 */
	errorHttp(erro) {
		console.log(erro);

		switch (erro.status) {
			case 0:
				this.app.infoAlert('Erro de Conexão', 'Não á conexão disponível');
				break;
			case 203:
				this.app.infoAlert('Erro de Autorização', 'O Servidor retornou um erro de Não Autorizado', 'Non-Authoritative Information');
				break;
			case 203:
				this.app.infoAlert('Erro de Autorização', 'O Servidor retornou um erro de Não Autorizado', 'Non-Authoritative Information');
				break;
			case 204:
				this.app.infoAlert('Erro de Conteúdo', 'O Servidor uma respota vazia', 'No Content');
				break;
			case 400:
				this.app.infoAlert('Erro de Solicitação Inválida', 'O Servidor retornou uma resposta inválida', 'Request');
				break;
			case 401:
				this.app.infoAlert('Erro de Não Autorizado', 'Servidor retornou um erro de não autorizado', 'Unauthorized');
				break;
			case 403:
				this.app.infoAlert('Erro de Proibido', 'O Servidor retornou acesso Proibido', 'Forbidden');
				break;
			case 404:
				this.app.infoAlert('Erro de Não Encontrado', 'O Servidor não encontrou o conteudo solicitado', 'Not Found');
				break;
			case 500:
				this.app.infoAlert('Erro de Servidor', 'O Servidor reportou Erro do Servidor Interno', 'Internal Server Error');
				break;
			case 502:
				this.app.infoAlert('Erro de Entrada', 'O Servidor retornou Porta de entrada ruim', 'Bad Gateway');
				break;
			case 503:
				this.app.infoAlert('Erro de Indisponibilidade', 'O Servidor reportou Serviço Indisponível', 'Service Unavailable');
				break;
			case 504:
				this.app.infoAlert('Erro de Time-Out', 'O Servidor retornou Tempo limite da Porta de Entrada', 'Gateway Time-out');
				break;
			default:
				this.app.infoAlert('Erro de Requisão', 'O Servidor retornou um erro não mapeado', 'HTTP: ' + erro.status + ' NAME:' + erro.name);
				break;
		}
	}
}