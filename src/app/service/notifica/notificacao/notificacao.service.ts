import { Injectable } from '@angular/core';
import { ConectaService } from '../../conecta/conecta.service';
import { AplicacaoService } from '../../aplicacao/aplicacao.service';

@Injectable({
	providedIn: 'root'
})
export class NotificacaoService {

	constructor(private conecta: ConectaService, private app : AplicacaoService) { }

	public notificacao = { qtd: 0, text: 'Nenhuma Notificação', lista: {} }
	
	async getAllNotify() {
		let notify = await this.conecta.get('SysNotificacao', 'getAllNotfy').then((response) => {			
			return response;
		})
		return notify;
	}

	async getQtdNotify() {
		let notify = await this.conecta.get('SysNotificacao', 'getQtdNotFlag').then((response) => {
			let aux: any = response;
			if (aux.qtd > 0) {
				//Define o texto do painel da notificacao, se vai ser plural ou nao
				if (aux.qtd == 1)
					this.notificacao.text = 'Notificação Não Lidas';
				else
					this.notificacao.text = 'Notificações Não Lidas';

				this.notificacao.qtd = aux.qtd;
			}
			return this.notificacao;
		}).catch((response) =>{
			this.app.trataErro(response)
		})
		return notify;
	}

	
	async setView(id){
		let dados = await this.conecta.get('SysNotificacao', 'setFlag&id=' + id).then((response) =>{
			return response;

		}).catch((response) => console.log(response));

		return dados;

	}
}
