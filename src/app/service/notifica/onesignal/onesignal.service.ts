import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AplicacaoService } from '../../aplicacao/aplicacao.service';

@Injectable({
	providedIn: 'root'
})
export class OnesignalService {

	public keysOneSignal = { appId: 'ba1a29c9-29c0-428a-8398-108fe196907e', key: '1036266723510', rest: 'YThjMTNhMGQtMTUyMC00ZjgyLWIwNDQtNzU4ODcxN2VlNTVi' }
	public notify: any = {}

	constructor(private oneSignal: OneSignal, private http: HttpClient, private app: AplicacaoService) { }

	/**
	   * Notificacao Push
	   * Utiliza a biblioteca oneSignal
	   * realiza as tratativas de receber notificacoes no app.
	   * Realiza ja as paranaues das permissoes.
	   */
	setupPush() {
		console.log('setupPush')
		this.oneSignal.startInit(this.keysOneSignal.appId, this.keysOneSignal.key);
		this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
		this.oneSignal.endInit();
	}

	/**
	 * Função para criação de notificações no servico de broadcast OneSignal
	 * Totalmente independete do servidor 
	 * @param titulo Titulo que deve conter na mensagem
	 * @param conteudo  Corpo da notificação
	 * @param icon nome do icone do aplicativa
	 * @param color cor do fundo do icone
	 * @param imglarge url publica de uma imagem a ser mostrada na notificacao
	 */
	async newNotify(titulo = 'Titulo', conteudo = 'Conteudo', icon = 'osdesk', color = '0D47A1', imglarge = null) {

		//Monta a estrutura do json a ser enviado para o servidor do servico OneSignal
		this.notify = {
			app_id: this.keysOneSignal.appId,
			included_segments: ["Active Users"],
			data: { "foo": "bar" },
			heading: { "en": titulo },
			contents: { "en": conteudo },
			small_icons: icon,
			android_accent_color: color,
			large_icon: imglarge
		}

		//Monta a estrutura dos Headers com a chave de autenticacao
		let headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8', 'Authorization': "Basic " + this.keysOneSignal.rest });

		let url = 'https://onesignal.com/api/v1/notifications';

		//Envia as notificacoes
		let send = await this.http.post(url, this.notify, { headers: headers }).toPromise().then((response) => {
			let r: any = response;

			//Caso tenha atingido os dispositivos
			if (r.recipients) {
				this.app.infoToast('Notificações enviadas para ' + r.recipients + ' dispositivos', 'success');
			} else {
				this.app.infoToast('Nenhum dispositivo recebeu a notificação', 'warning');

			}
		}).catch((response) => {
			console.log(response)
		})
		return send;
	}

	async getHistory(limit = 50) {
		//Monta a estrutura dos Headers com a chave de autenticacao
		let headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8', 'Authorization': "Basic " + this.keysOneSignal.rest });

		let url = 'https://onesignal.com/api/v1/notifications?app_id=' + this.keysOneSignal.appId + '&limit=' + limit;

		//Envia as notificacoes
		return await this.http.get(url, { headers: headers }).toPromise();

	}
}
