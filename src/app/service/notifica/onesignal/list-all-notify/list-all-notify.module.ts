import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListAllNotifyPage } from './list-all-notify.page';

const routes: Routes = [
  {
    path: '',
    component: ListAllNotifyPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListAllNotifyPage]
})
export class ListAllNotifyPageModule {}
