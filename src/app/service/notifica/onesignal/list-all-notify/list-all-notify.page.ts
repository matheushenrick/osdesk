import { Component, OnInit } from '@angular/core';
import { NotificacaoService } from '../../notificacao/notificacao.service';
import { LoadingController } from '@ionic/angular';
import { AplicacaoService } from 'src/app/service/aplicacao/aplicacao.service';

@Component({
	selector: 'app-list-all-notify',
	templateUrl: './list-all-notify.page.html',
	styleUrls: ['./list-all-notify.page.scss'],
})
export class ListAllNotifyPage implements OnInit {

	public notificacoes: any = {};

	constructor(private server: NotificacaoService, private loadingCtrl: LoadingController, public app : AplicacaoService) { }

	ngOnInit() {
		this.showNotify();
	}

	/**
	 * Busca todas as notificações do servidor 
	 */
	async showNotify() {
		let loading = await this.loadingCtrl.create({
			message: 'Buscas as notificações',
			spinner: "bubbles",
			translucent: false
		})

		loading.present();

		let dados = await this.server.getAllNotify().then((response) => {
			this.notificacoes = response

		}).finally(() => loading.dismiss());

		return dados
	}
	
	async setView(id){
		await this.server.setView(id);
		this.app.infoToast('Marcado como Visualizado', 'success', 1000);
	}	
}
