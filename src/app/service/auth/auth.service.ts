import { Injectable } from '@angular/core';
import { MenuController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ConectaService } from '../conecta/conecta.service';
import { AplicacaoService } from '../aplicacao/aplicacao.service';
import { ConfigService } from '../config/config/config.service';
@Injectable({
	providedIn: 'root'
})
/**
 * Classe de servicos de autenticacao
 * Conecta com o backend informando os dados sensiveis do usuario e obtem a resposta de autenticado ou nao, caso esteja autenticado
 * retorna junto os detalhes do usuario
 * Aqui tem o controle autenticacao por meio da Biometria
 * Matheus Henrique
 */
export class AuthService {

	private dadosUser: any;

	constructor(private conecta: ConectaService, private storage: Storage, private menu: MenuController, private aplicacao: AplicacaoService, private config: ConfigService, private loading: LoadingController) { }

	/**
	 * Funcao de autenticacao do usuario
	 * @param user Login a ser autenticado
	 * @param pswd Senha
	 * @param emp Empresa
	 */
	async autenticaUser(user: string, pswd: string, emp = '') {

		//Mostra a mensagem de autenticando
		const load = await this.loading.create({
			message: 'Autenticando Usuário',
			spinner: 'dots'
		});

		load.present();

		let usuario = { str_login: user, str_senha: btoa(pswd) };

		this.conecta.enviaDados('usuario', 'autenticaUser', usuario, false, emp).then(async (response) => {
			this.dadosUser = response;
			//Caso tenha autenticado com sucesso
			if (this.dadosUser.autenticado) {
				//Cria o registro do usuario
				this.storage.set('dadosUser', this.dadosUser.dados);
				//Define a empresa 
				this.aplicacao.setFilial(emp);
				//Desbloqueia o menu
				this.menu.enable(true);

				//Remove o abiso de autenticando e redireciona para a home
				await load.dismiss().then(() => {
					this.aplicacao.navegar('home');
				})
			}
			//Alerta de nao foi possivel autenticar
			else {
				load.dismiss();
				this.aplicacao.infoAlert('Usuário e/ou senha incorretos', 'verifique os dados e tente novamente');
			}
			//Caso nao tenha conseguido conectar com o backend.
		}).catch((response) => {
			console.log(response);
			load.dismiss();

			this.aplicacao.infoAlert('Ops!', 'Algo de errado não esta certo', 'Erro na requisição com o servidor');
		})
	}

	//Realiza o processo de Logout
	logout() {
		//Bloqueia o menu
		this.menu.enable(false);
		//Redireciona para a tela de login
		this.aplicacao.navegar('login');
		//Remove os dados de Usuario
		this.storage.remove('dadosUser');
		//Remove os dados da Filial
		this.storage.remove('EMP');
	
	}

	//Retorna os dados do usuario
	async getUserByDB() {
		let dados = await this.storage.get('dadosUser').then((response) =>{
			return response
		});
		return dados;
	}

	//Remove os dados de autenticacao
	removeDadosUser() {
		this.storage.remove('biometricAuthenticationData');
	}
}
