import { Injectable } from '@angular/core';
import { ConectaService } from '../conecta/conecta.service';

@Injectable({
	providedIn: 'root'
})
export class WeatherService {

	//Chave de autenticacao com o servido HgBrasil
	public chave = '0b10b267';

	public gps: any;
	public coord = { lat: '', lon: '' };
	public server: any;
	public ip: any;

	public clima: any = { result: { temp: '', description: '', city: '' } };

	constructor(private conecta: ConectaService) { }

	async getWeather() {
		// await this.getGPS();

		await this.getIPbyServer();

		let url = 'https://api.hgbrasil.com/weather/?format=json-cors'
			+ '&user_ip=' + this.ip
			+ '&key=' + this.chave;

		let clima = await this.conecta.getURL(url).then((response) => {
			this.clima = response
			return this.clima.results;
		}).catch((response) =>{
			console.log(response);
		});

		return clima;
	}

	async getIPbyServer() {
		let ip = await this.conecta.serverInfo().then(async (response) => {
			this.server = response
			this.ip = this.server.REMOTE_ADDR
		});

		return ip;
	}
}
