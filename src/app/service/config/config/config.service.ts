import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AplicacaoService } from '../../aplicacao/aplicacao.service';


@Injectable({
	providedIn: 'root'
})
export class ConfigService {
	public authBiometria = false;

	public dadosReq = {
		url_server: 'localhost',
		port_server: '80'
	}
	public dadosAuth = {
		acToken: false,
		authToken: '',
		acID: false,
		idToken: ''
	}

	public aparelho = {
		manufacturer: '',
		model: '',
		platform: '',
		serial: '',
		uuid: '',
		version: '',
		cordova: ''
	}

	constructor(public storage: Storage, public app: AplicacaoService) { }

	async salvaConfig(dadosAuth?, dadosReq?, authBiometria?, redirect = true, toast = true, empresa?) {
		this.storage.get('configAPP').then((response) => {
			if (response == null) {
				this.storage.set('configAPP', { dadosAuth: (dadosAuth ? dadosAuth : this.dadosAuth), dadosReq: (dadosReq ? dadosReq : this.dadosReq), biometria: (authBiometria ? authBiometria : this.authBiometria) }).then((response) => {
					if (toast)
						this.app.infoToast('Configurações Salvas com Sucesso!', 'success', 1000);
				}).catch(() => {
					if (toast)
						this.app.infoToast('Erro ao salvar as configurações', 'danger', 1000);
				});
			}
		})
		if(empresa != '')
			this.salvaDadosEmp(empresa, false, false);
		
		if (redirect)
			this.app.navegar('home');
	}

	async salvaDadosEmp(dadosEmp, toast = false, redirect = false) {
		this.storage.get('configDadosEmp').then((response) => {
			if (response == null) {
				this.storage.set('configAPP', { dadosEmp : dadosEmp }).then((response) => {
					if (toast)
						this.app.infoToast('Configurações de Empresa Salvas com sucesso!', 'success', 1000);
				}).catch(() => {
					if (toast)
						this.app.infoToast('Erro ao salvar as configurações', 'danger', 1000);
				});
			}
		})
		if (redirect)
			this.app.navegar('/home');
	}

	getURLServer() {
		this.storage.get('configAPP').then((response) => {
			if (response != undefined) {
				let dados = response;
				return dados.dadosReq.url_server
			} else {
				return null;
			}
		})
	}

	getPORTServer() {
		this.storage.get('configAPP').then((response) => {
			if (response != undefined) {
				let dados = response;
				return dados.dadosReq.port_server
			} else {
				return null;
			}
		});
	}

	getServer() {
		this.storage.get('configAPP').then((response) => {
			if (response != undefined) {
				let dados = response;
				return dados.dadosReq.url_server + ':' + dados.dadosReq.port_server
			} else {
				return null;
			}
		});
	}

	async getBioEnable() {
		let bio = await this.storage.get('configAPP').then((response) => {
			if (response != undefined) {
				let dados = response;
				return dados.biometria;
			} else {
				return false;
			}
		});
		return await bio;
	}

	setBioEnable(status) {
		this.storage.get('configAPP').then((response) => {
			if (response != undefined) {
				let dados = response;
				dados.biometria = status;
				this.storage.set('configAPP', dados);
			} else {
				return null;
			}
		});
	}

	getAuthConf() {
		this.storage.get('configAPP').then((response) => {
			if (response != undefined) {
				let dados = response;
				return dados.dadosAuth
			} else {
				return null;
			}
		});
	}
}
