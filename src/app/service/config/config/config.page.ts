import { ConfigService } from './config.service';
import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Device } from '@ionic-native/device/ngx';
import { Router } from '@angular/router';
import { AplicacaoService } from '../../aplicacao/aplicacao.service';

@Component({
	selector: 'app-config',
	templateUrl: './config.page.html',
	styleUrls: ['./config.page.scss'],
})

export class ConfigPage implements OnInit {
	public authBiometria = false

	public dadosReq = {
		url_server: 'localhost',
		port_server: '80'
	}

	public dadosEmp = {
		cod_emp : '000001',
		nome_emp : 'Administrador',
		cnpj: '01.261.978/0001-65'
	}
	
	public dadosAuth = {
		acToken: false,
		authToken: '',
		acID: false,
		idToken: ''
	}

	public aparelho = {
		manufacturer: '',
		model: '',
		platform: '',
		serial: '',
		uuid: '',
		version: '',
		cordova: ''
	}

	constructor(public deviceCtrl : Device, public plataform : Platform, public config : ConfigService, public app : AplicacaoService, public routerCtl : Router) { }

	ngOnInit() {
		this.plataform.ready().then(() => {
			this.aparelho.manufacturer = this.deviceCtrl.manufacturer;
			this.aparelho.model = this.deviceCtrl.model;
			this.aparelho.platform = this.deviceCtrl.platform;
			this.aparelho.serial = this.deviceCtrl.serial;
			this.aparelho.uuid = this.deviceCtrl.uuid;
			this.aparelho.version = this.deviceCtrl.version;
			this.aparelho.cordova = this.deviceCtrl.cordova;
		});
	}

	salvaDadosServer(){
		this.config.salvaConfig(this.dadosAuth, this.dadosReq, this.authBiometria, true);
		this.salvaDadosEmp();
	}

	salvaDadosEmp(){
		this.config.salvaDadosEmp(this.dadosEmp);
	}

	sairConfig(){
		this.app.infoToast('Nenhuma configuração foi salva', 'primary', 1000);
		this.routerCtl.navigateByUrl('/home');
	}

	

	
}
