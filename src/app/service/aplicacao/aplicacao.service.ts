import { ToastController, AlertController, LoadingController, Platform, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ErrorService } from '../error/error.service';

/**
 * aplicacao.service.ts
 * Um services que entrega varias (no momento poucas) funcoes sem ter que ficar copiando e colando ou enchendo os fonts de funcoes
 * Autor Matheus Henrique
 * Data: Uma das primeiras ;(
 */
export class AplicacaoService {

	constructor(private toastCtrl: ToastController, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private routerCtrl: Router, private storage: Storage, private erroSrv : ErrorService) { }

	/**
	 * infoToast
	 * Cria um toast na pagina, utilizar bastante, é bonitinho
	 * @param texto Texto a ser mostrado no toast
	 * @param color tipo de mesagem, danger, success, warning
	 * @param time tempo de exibição na tela (em ms);
	 */
	async infoToast(texto, color = 'danger', time = 3000, local = 'bottom') {

		let toast = await this.toastCtrl.create({
			message: texto,
			duration: time,
			position: local == 'bottom' ? 'bottom' : 'top',
			color: color
		});
		return await toast.present();
	}

	/**
	 * infoAlert
	 * Cria um alert estilizado e padrao ionic - diferente do alert javascript - 
	 * utilizar para alertar importantes e que devem ser lidos
	 * @param header Titulo do alert
	 * @param subHeader Subtitulo do Alert
	 * @param message Mensagem de explicacao
	 */
	async infoAlert(header = '', subHeader = '', message = '', btns?) {
		let alert = await this.alertCtrl.create({
			header: header,
			subHeader: subHeader,
			message: message,
			buttons: btns ? btns : [{ text: 'Ok', handler: () => alert.dismiss() }]
		});
		return await alert.present();
	}

	/**
	 * loading
	 * Cria um loading. Simples né?!
	 * @param text Texto a ser mostrado no loading
	 * @param time Tempo de loading
	 */
	async loading(text: any, time = 300) {
		let loading = await this.loadingCtrl.create({
			message: text,
			duration: time
		});
		return await loading.present();
	}

	/**
	 * Esta funcao é magica, para nao ter que ficar importando router e decorando os detalhes de navegacao
	 * @param url informar a pagina SEM A BARRA
	 */
	navegar(url) {
		return this.routerCtrl.navigateByUrl('/' + url);
	}

	/**
	 * Retorna a Filial salva
	 * Utilizar como no exemplo a seguin
	 * await this.app.getFilial().then((response)=>{
	 *		emp = response;	
	 * }); emp.Filial
	 */
	async getFilial() {
		let fil = '';

		await this.storage.get('Filial').then(async (response) => {
			console.log(response);
			fil = response
		}).catch((response) => {
			console.log(response);
		})
		return fil;
	}

	/**
	 * Define uma filial a ser salva
	 * @param fil Filial
	 */
	async setFilial(fil) {
		let dados = '';
		await this.storage.set('Filial', { Filial: fil }).then(async (response) => {
			dados = response
		});
		return dados;
	}

	/**
	 * Para Obter a data atual
	 * @param tipo US '2019-10-10' ou BR '10/10/2019'
	 */
	getDate(tipo = 'US') {
		var dNow = new Date();
		let date: any;

		if (tipo == 'US')
			date = dNow.getFullYear() + '-' + (dNow.getMonth() + 1) + '-' + dNow.getDate();
		if (tipo == 'BR')
			date = dNow.getDate() + '/' + (dNow.getMonth() + 1) + '/' + dNow.getFullYear();

		return date;
	}

	/**
	 * Retorna a Hora Atual do Sistema
	 * 11:04
	 */
	getHora() {
		var dNow = new Date();
		let hora: any;

		hora = dNow.getHours() + ':' + dNow.getMinutes();
		return hora;
	}

	/**
	 * Realiza a conversao da data no formato 2019-11-26 para 26/11/2019
	 * Pode conter hora tambem 2019-11-26 13:56:00 para 26/11/2019 13:56:00
	 * @param d data a ser convertida
	 */
	dtoc(d) {
		let dataHora = d.split(' ');
		
		//Verifica se tem hora junto a data
		let hora;
		if (dataHora.length > 0)
			hora = dataHora[1];

		let data = dataHora[0].split('-');
		return data[2] + '/' + data[1] + '/' + data[0] + ' ' + hora;
	}

	/**
	 * Verifica se algum campo esta vazio
	 * @param valor array, obj ou string a ser verificada
	 */
	empty(valor) {
		if (Array.isArray(valor)) {
			console.log('array');
			valor.forEach(element => {
				console.log(element);
				if (element == undefined || element == '' || element == null)
					return element;
			});
		} else if (valor instanceof Object && !(valor instanceof Array)) {
			console.log('obj');
			for (var [key, value] of Object.entries(valor)) {
				if (value == undefined || value == '' || value == null)
					return key;
			}
		} else {
			console.log('string');
			if (valor == undefined || valor == '' || valor == null)
				return true;
		}
		return false;
	}

	/**
	 * Verifica se o parametro passado é um obj
	 * @param valor 
	 */
	isObj(valor) {
		if (valor instanceof Object && !(valor instanceof Array)) {
			return true
		}
		return false;
	}

	/**
	 * Funcao para limpar os valores de um obj
	 * @param obj 
	 */
	limpaObj(obj) {
		if (obj instanceof Object && !(obj instanceof Array)) {
			for (var [key] of Object.entries(obj)) {
				obj[key] = '';
			}
			return obj;
		}
	}

	async trataErro(response) {
		await this.erroSrv.trataErro(response);
	}
	
}
