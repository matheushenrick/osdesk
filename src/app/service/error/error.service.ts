import { Injectable } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class ErrorService {

	constructor(private alertCtrl: AlertController, private nvgt: Router) { }

	async trataErro(response) {
		console.log(response);
		let alert = await this.alertCtrl.create({
			header: 'Erro inexperado!',
			message: 'Desculpe pelo incoveniente, algo não planejado acabou de acontecer. O Aplicativo será reiniciado!',
			mode : 'ios',
			backdropDismiss : true,

			buttons: [{
				text: 'Tudo Bem =(', 
				cssClass: 'secondary',
				role: 'cancel',
				handler: () => {
					this.nvgt.navigateByUrl('login');
				}
			}, {
				text: 'Reportar', cssClass: 'danger', handler: async () => {
					let reportar = await this.alertCtrl.create({
						header : 'Reportar Evento',
						message : 'Enviado ao Desenvolvedor os logs de eventos',
						buttons : [{text : 'Sair', handler : ()=>{
							this.nvgt.navigateByUrl('login')
						}}]
					})
					reportar.present();
				}
			}]
		})
		alert.present();
	}
}
