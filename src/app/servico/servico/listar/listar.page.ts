import { Component, OnInit } from '@angular/core';
import { ConectaService } from 'src/app/service/conecta/conecta.service';
import { AlertController } from '@ionic/angular';
import { AplicacaoService } from 'src/app/service/aplicacao/aplicacao.service';

@Component({
	selector: 'app-listar',
	templateUrl: './listar.page.html',
	styleUrls: ['./listar.page.scss'],
})
export class ListarPage implements OnInit {
	
	private controller = 'osdesk005';

	constructor(private conecta: ConectaService, private alert: AlertController, private app: AplicacaoService) { }

	public envio : any  = {str_desc : '', str_equipe : ''};
	public arrDispo: any;
	public nenhumRegistro: boolean = true;
	public equipes: any;

	ngOnInit() {
		this.getDados();
		this.getEquipes();
	}

	async salvaDados() {

		if (this.app.empty(this.envio))
			this.app.infoAlert('Campos Obrigatórios', 'Campos obrigatórios nao preenchidos!');
		else {
			let dados = await this.conecta.inclui(this.controller, this.envio).then((response) => {
				if (response['erro'] == 1) {
					this.app.infoAlert('Erro de Inserção', response['retorno']);
				}
				else {
					this.app.infoToast('Incluido com Sucesso', 'success');
					this.app.limpaObj(this.envio);

					this.getDados();
				}
			}).catch((response)=>{
				console.log(response);
			})
			return dados;
		}
	}

	async getDados() {
		let dados = await this.conecta.get(this.controller, 'returnAllResults').then((response) => {
			this.arrDispo = response;
			try {
				if (this.arrDispo.length > 0)
					this.nenhumRegistro = false;
			} catch (error) {
				this.nenhumRegistro = true;
			}
		}).catch((response)=>{
			console.log(response);
		})
		return dados;
	}

	async getEquipes() {

		let retornaEquipe = await this.conecta.get(this.controller, 'retornaEquipe').then(async (response) => {
			this.equipes = response;
			if (this.equipes.length == 0) {
				let alert = await this.alert.create({
					header: 'Cadastro de Serviços',
					message: 'Nenhuma equipe cadastrada, por favor cadastre para continuar',
					buttons: [{
						text: 'Cancelar',
						role: 'cancel',
						cssClass: 'secondary',
						handler: () => {
							this.app.navegar('home');
						}
					}, {
						text: 'Cadastrar Equipe',
						handler: () => {
							this.app.navegar('list_equipe');
						}
					}]
				})
				alert.present();
			}

		}).catch((response)=>{
			console.log(response);
		});
		return retornaEquipe;
	}

	async excluir(item) {
		let exclui = await this.alert.create({
			header: 'Excluir',
			message: 'Deseja Excluir o registro: ' + item.str_desc + ' da equipe ' + item.str_equipe + '?',
			buttons: [{
				text: 'Cancelar',
				role: 'cancel',
				cssClass: 'secondary'
			}, {
				text: 'Sim, Excluir',
				handler: () => {
					this.conecta.exclui(this.controller, item.id).then(async (response) => {
						if (response['erro'] == 1)
							this.app.infoAlert('Erro de Exclusão', response['retorno']);
						else {
							this.app.infoToast('Excluido com Sucesso', 'success');
							this.getDados();
						}
					}).catch((response)=>{
						console.log(response);
					});
				}
			}]
		})
		exclui.present();
		return exclui;
	}

}
