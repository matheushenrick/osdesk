import { Component, OnInit } from '@angular/core';
import { ConectaService } from 'src/app/service/conecta/conecta.service';

@Component({
	selector: 'app-cadastro',
	templateUrl: './cadastro.page.html',
	styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

	controller = 'produto';
	form: any;
	envio = [];

	constructor(private conecta: ConectaService) { }

	ngOnInit() {
		this.preencheForm();
	}


	async preencheForm() {
		let dados = await this.conecta.get('card', 'camposByCard', 'tab=produto').then((response) => {
			this.form = response;
		})

		await this.form.forEach(async (element) => {
			element.campos.forEach(p => {
				this.envio[p.str_campo] = '';
			});
		});

		return dados;
	}

	async salvar() {
		console.log(this.envio);
		// let dados 

		// let dados = await this.conecta.inclui(this.controller, { str_nome: this.str_nome }).then((response) => {
		// 	if (response['erro'] == 1) {
		// 		this.app.infoAlert('Erro de Inserção', response['retorno']);
		// 		this.str_nome = '';
		// 	}
		// 	else {
		// 		this.app.infoToast('Incluido com Sucesso', 'success');
		// 		this.str_nome = '';
		// 		this.getDados();
		// 	}
		// }).catch((response) =>{
		// 	console.log(response);
		// })
		// return dados;
	}
}
