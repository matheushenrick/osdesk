import { Component, OnInit } from '@angular/core';
import { ConectaService } from 'src/app/service/conecta/conecta.service';
import { AlertController } from '@ionic/angular';
import { AplicacaoService } from 'src/app/service/aplicacao/aplicacao.service';

@Component({
	selector: 'app-unidade',
	templateUrl: './unidade.page.html',
	styleUrls: ['./unidade.page.scss'],
})
export class UnidadePage implements OnInit {

	constructor(private conecta: ConectaService, private alert: AlertController, private app: AplicacaoService) { }

	public str_sigla: string;
	public str_desc: string;
	public arrUnid: any;
	public nenhumRegistro: boolean = true;
	public controller = 'osdesk002';


	ngOnInit() {
		this.getDados();
	}

	async salvaDados() {

		if (this.app.empty(this.str_sigla) && this.app.empty(this.str_desc))
			this.app.infoAlert('Campos Obrigatórios', 'O Campo Nome não pode ser vazio!');
		else {
			let dados = await this.conecta.inclui(this.controller, { str_sigla: this.str_sigla, str_desc: this.str_desc }).then((response) => {
				if (response['erro'] == 1) {
					this.app.infoAlert('Erro de Inserção', response['retorno']);
					this.str_sigla = '';
					this.str_desc = '';
				}
				else {
					this.app.infoToast('Incluido com Sucesso', 'success');
					this.str_sigla = '';
					this.str_desc = '';
					this.getDados();
				}
			})
			return dados;
		}
	}

	async getDados() {
		let dados = await this.conecta.get(this.controller, 'returnAllResults').then((response) => {
			this.arrUnid = response;
			try {
				if (this.arrUnid.length > 0)
					this.nenhumRegistro = false;
			} catch (error) {
				this.nenhumRegistro = true;
			}
		})

		return dados;
	}

	async excluir(item) {
		let exclui = await this.alert.create({
			header: 'Excluir',
			message: 'Deseja Excluir o registro: ' + item.str_nome + ' ?',
			buttons: [{
				text: 'Cancelar',
				role: 'cancel',
				cssClass: 'secondary'
			}, {
				text: 'Sim, Excluir',
				handler: () => {
					this.conecta.exclui(this.controller, item.id).then(async (response) => {
						if (response['erro'] == 1)
							this.app.infoAlert('Erro de Inserção', response['retorno']);
						else {
							this.app.infoToast('Excluido com Sucesso', 'success');
							this.getDados();
						}
					});
				}
			}]
		})
		exclui.present();
		return exclui;
	}
}
