import { Component, OnInit } from '@angular/core';
import { AplicacaoService } from 'src/app/service/aplicacao/aplicacao.service';
import { MenuController } from '@ionic/angular';
import { AuthService } from 'src/app/service/auth/auth.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.page.html',
	styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

	public urlImg = '../../assets/img/logo.png'
	constructor(private app: AplicacaoService, private menu: MenuController, private auth: AuthService) { }

	public user = { str_login: '', str_senha: '', str_filial : 'ADM' };

	ngOnInit() {
		this.menu.enable(false);
	}

	async login() {
		if(!this.app.empty(this.user)){
			let login = await this.auth.autenticaUser(this.user.str_login, this.user.str_senha, this.user.str_filial);
			return login;

		}else{
			this.app.infoToast('Preencha os campos de usuário e senha para continuar', 'danger');
		}
  }

}
