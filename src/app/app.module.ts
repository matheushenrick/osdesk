import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ConectaService } from './service/conecta/conecta.service';
import { AplicacaoService } from './service/aplicacao/aplicacao.service';
import { HttpClientModule } from '@angular/common/http';
import { Device } from '@ionic-native/device/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { WeatherService } from './service/weather/weather.service';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { ErrorService } from './service/error/error.service';
import { OrdermservicoService } from './ordemservico/ordermservico.service';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { AcompanharPageModule } from './ordemservico/acompanhar/acompanhar/acompanhar.module';
import { BrMaskerModule } from 'br-mask';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    AppRoutingModule,
    FilterPipeModule,
    AcompanharPageModule,
    BrMaskerModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ConectaService, // Conexão
    AplicacaoService, // Lib personalizada
    WeatherService, //clima
    ErrorService, // Tratar erros
    Device, // Carrega dados do dispositivo
    OrdermservicoService, // Ordem de Servicos
    Platform,
    OneSignal,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }