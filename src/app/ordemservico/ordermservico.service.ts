import { Injectable } from '@angular/core';
import { ConectaService } from '../service/conecta/conecta.service';
import { LoadingController } from '@ionic/angular';
import { AplicacaoService } from '../service/aplicacao/aplicacao.service';

@Injectable({
	providedIn: 'root'
})
export class OrdermservicoService {

	public osHome: any = {};
	constructor(private conecta: ConectaService, private loadingCtrl: LoadingController, private app: AplicacaoService) { }

	/**
	 * Funcao utilizada na Home
	 * Retorna um resumo de todas as OS
	 * Utilizar somente na home, porque tem qts e listagem limitada
	 */
	async getDadosOS() {
		let loading = await this.loadingCtrl.create({
			message: 'Carregando Informações das Ordens de Serviços',
			mode: 'ios',
			spinner: 'bubbles'
		})

		loading.present();

		/**
		 * Busca os dados das OS Abertas
		 */
		await this.conecta.get('osdesk008', 'getOSOpen', 'qtd').then((response) => {
			this.osHome.qtdAberto = response;
		}).catch((response) => this.conecta.errorHttp(response)).finally(() => {
			this.conecta.get('osdesk008', 'getOSOpen', 'limit=5').then((response) => {
				this.osHome.osAbertas = response
			}).catch((response) => this.conecta.errorHttp(response))
		})

		/**
		 * Busca dados das OS Em Andamento
		 */
		await this.conecta.get('osdesk008', 'getOSRun', 'qtd').then((response) => {
			this.osHome.qtdAndamento = response;
		}).catch((response) => this.conecta.errorHttp(response)).finally(() => {
			this.conecta.get('osdesk008', 'getOSRun', 'limit=5').then((response) => {
				this.osHome.osAndamento = response
			}).catch((response) => this.conecta.errorHttp(response))
		})

		/**
		 * Busca dados das OS Em Encerradas
		 */
		await this.conecta.get('osdesk008', 'getOSClose', 'qtd').then((response) => {
			this.osHome.qtdEncerrados = response;
		}).catch((response) => this.conecta.errorHttp(response)).finally(() => {
			this.conecta.get('osdesk008', 'getOSClose', 'limit=5').then((response) => {
				this.osHome.osEncerrados = response
			}).catch((response) => this.conecta.errorHttp(response))
		})

		loading.dismiss();

		return this.osHome;
	}

	/**
	 * Busca os dados das OS Abertas
	*/
	async geOsOpen() {

		let open = await this.conecta.get('osdesk008', 'getOSOpen').then((response) => {
			return response
		}).catch((response) => this.conecta.errorHttp(response));

		return open;
	}

	/**
	 * Busca os dados das OS Encerradas
	*/
	async getOSClose() {

		let open = await this.conecta.get('osdesk008', 'getOSClose').then((response) => {
			return response
		}).catch((response) => this.conecta.errorHttp(response));

		return open;
	}

	/**
	 * Busca os dados das OS Andamento
	*/
	async getOSRun() {

		let open = await this.conecta.get('osdesk008', 'getOSRun').then((response) => {
			return response
		}).catch((response) => this.conecta.errorHttp(response));

		return open;
	}

	/**
	 * Busca os dados das OS Em Avaliação
	 */
	async getOSAvaliacao() {

		let open = await this.conecta.get('osdesk008', 'getOSAvaliacao').then((response) => {
			return response
		}).catch((response) => this.conecta.errorHttp(response));

		return open;
	}

	/**
	 * Busca os dados das OS Andamento
	*/
	async getOSPendind() {

		let open = await this.conecta.get('osdesk008', 'getOSPendind').then((response) => {
			return response
		}).catch((response) => this.conecta.errorHttp(response));

		return open;
	}

	/**
	 * Encaminha a os para a fila de um analista
	 * @param resp id Analista Responsavel
	 * @param os id Ordem de Servico
	 */
	async encaminhaOS(resp, os) {
		if (this.app.empty(resp)) {

		} else {
			let dados = { id_os: os.id, id_analista: resp }
			this.conecta.enviaDados('osdesk008', 'setAnalistaByOs', dados).then(() => {
				this.app.infoToast('Sucesso ao Encaminhar a Ordem de Serviço', 'success',500);
			}).catch((response) => {
				console.log(response);
			})
		}
	}

	/**
	 * Inicia o atendimento da ordem de servico
	 * @param os id da ordem de servico
	 */
	async iniciarAtendimento(os){
		let dados = { id_os: os.id }
			this.conecta.enviaDados('osdesk008', 'iniciarAtendimento', dados).then(() => {
				this.app.infoToast('Sucesso ao Aceitar Ordem de Serviço', 'success', 500);
			}).catch((response) => {
				console.log(response);
			})
	}

	/**
	 * Finaliza a ordem de servico e deixa em pendente de avaliacao
	 * @param os is da os
	 */
	async FinalizarAtendimento(os){
		let dados = { id_os: os.id }
			this.conecta.enviaDados('osdesk008', 'FinalizarAtendimento', dados).then((response) => {
				console.log(response);
			}).catch((response) => {
				console.log(response);
			})
	}

	/**
	 * Busca todos os usuarios que estao no grupo de analista
	 */
	async getAnalista() {
		let dados = { grupo: 'analista-app-ADM' };
		let getAnal = await this.conecta.enviaDados('osdesk008', 'getAnalistas', dados).then((response) => {
			return response;
		}).catch((response) => {
			console.log(response)
		})

		return getAnal;
	}
}
