import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, LoadingController } from '@ionic/angular';
import { ConectaService } from 'src/app/service/conecta/conecta.service';
import { AplicacaoService } from 'src/app/service/aplicacao/aplicacao.service';
import { AuthService } from 'src/app/service/auth/auth.service';

@Component({
	selector: 'app-acompanhar',
	templateUrl: './acompanhar.page.html',
	styleUrls: ['./acompanhar.page.scss'],
})
export class AcompanharPage implements OnInit {
	public dateMax: any;

	constructor(private navparam: NavParams, private conecta: ConectaService, private modalCtrl: ModalController, public app: AplicacaoService, private auth: AuthService, private loadingCtrl: LoadingController) {
		this.dateMax = (new Date()).toISOString();
	}

	public dadosUser: any;
	public typeAcao: any = {};
	public dadosOS: any = {}
	public acompanhamentos: any;
	public novoAcompanhamento = [];

	public avaliacao = { int_aval1: 0, int_aval2: 0, int_aval3: 0, int_aval4: 0 };

	async ngOnInit() {
		let loading = await this.loadingCtrl.create({
			message: 'Busca dados de acompanhamanto da Ordem de Serviço',
			mode: 'ios',
			spinner: 'bubbles'
		})
		loading.present().then(async () => {
			// Busca os dados do usuario
			await this.auth.getUserByDB().then((response) => this.dadosUser = response);
			// Busca os dados da Ordem de Servico
			this.dadosOS = await this.navparam.get('dados_os');
			// Busca os dados dos acompanhamentos anteriores.
			await this.getAcompanhamento();
			// Busca e define a acao da pagina, se vai ser acompanhamento ou se vai ser finalização
			await this.setTypeAcao();

		});

		await loading.dismiss();
	}

	/**
	 * Busca os dados de acompanhamentos anteriores
	 */
	async getAcompanhamento() {
		let acomp = await this.conecta.get('osdesk010', 'getAcompanhamentobyId', 'id_os=' + this.dadosOS.id).then((response) => {
			this.acompanhamentos = response;
		})
		return acomp;
	}

	/**
	 * Adiciona novos campos para acompanhamento da OS
	 */
	async addAcompanhamento() {
		this.novoAcompanhamento.push({ str_tempo: '', str_comentario: '', data_relato: '', id_osdesk008: this.dadosOS.id, dat_data: this.app.getDate('BR'), str_hora: this.app.getHora(), id_usuario: this.dadosUser.id });
	}

	/**
	 * Fecha o modal
	 */
	closeModal() {
		this.modalCtrl.dismiss();
	}

	/**
	 * Define o tipo de acao a ser executada na pagina
	 * se for acompanhamento, o usuario ve os acompanhamentos anteriores e realiza um novo
	 * se for encerramento é obrigatorio fazer um acompanhamento e encerra a OS
	 */
	async setTypeAcao() {
		let acao = await this.navparam.get('acao');

		if (acao == 'finaliza') {
			this.typeAcao = { acao: 'finaliza', textHeader: 'Finalizar O.S. Nº ', textBody: 'Finalizar a ação exige que você faça um acompanhamento da ação realizada para encerrar a ordem de serviço.', textAction: 'Finalizar' };
			this.addAcompanhamento();
		} else if (acao == 'acompanha') {
			this.typeAcao = { acao: 'acompanha', textHeader: 'Acompnanhar O.S. Nº ', textBody: 'Informe os acompanhamentos desta ordem de serviço, cada detalhe é muito importante!', textAction: 'Acompanhamento' };
		} else if (acao == 'avaliar') {
			this.typeAcao = { acao: 'avaliar', textHeader: 'Avaliar O.S. Nº ', textBody: 'Avalie sua experiência com este atendimento, cada detalhe é muito importante para sempre melhor lhe atender!', textAction: 'Avaliação' };
		}
	}

	/**
	 * Salva os acompanhamento
	 * quando é um acompanhamento simples apenas salva os dados
	 * se for encerramento faz o acompanhamento e altera o status para finalizado
	 */
	async salvarAcompanhamento() {
		console.log(this.typeAcao.acao);
		if (this.typeAcao.acao != 'avaliar') {
			let loading = await this.loadingCtrl.create({
				message: 'Enviando dados de Acompanhamento',
				mode: 'ios',
				spinner: 'bubbles'
			})

			loading.present().then(async () => {
				//Caso seja acompanhamento
				if (this.typeAcao.acao == 'acompanha') {
					let send = await this.conecta.enviaDados('osdesk010', 'processaAcompanhamento', this.novoAcompanhamento).then((response) => {
						this.app.infoToast('Acompanhamentos Salvos com Sucesso!', 'success');
					})
						.catch((response) => this.conecta.errorHttp(response))
						.finally(() => {
							this.closeModal();
							loading.dismiss();
						})
					return send;

					// Caso seja finalizacao
				} else if (this.typeAcao.acao == 'finaliza') {
					let send = await this.conecta.enviaDados('osdesk010', 'processaAcompanhamento', this.novoAcompanhamento).then(async (response) => {
						let final = await this.conecta.get('osdesk010', 'finalizaOsById', 'id_os=' + this.dadosOS.id).then((response) => {
							this.app.infoToast('Ordem de Serviço finalizada com sucesso!', 'success');
						})

						return final;
					})
						.catch((response) => this.conecta.errorHttp(response))
						.finally(() => {
							this.closeModal();
							loading.dismiss();
						})
					return send;
				}
			})
		} else {
			console.log(this.avaliacao);
			let loading = await this.loadingCtrl.create({
				message: 'Enviando dados de Avaliação',
				mode: 'ios',
				spinner: 'bubbles'
			})

			loading.present().then(async () => {
				let final = await this.conecta.enviaDados('osdesk010', 'avaliaOsById&id_os=' + this.dadosOS.id, this.avaliacao).then((response) => {

					this.app.infoToast('Ordem de Serviço Avaliada com sucesso!', 'success');
				}).catch((response) =>{
					this.conecta.errorHttp(response);
					loading.dismiss();
				})

				return final;
			}).finally(() =>{
				loading.dismiss();
				this.closeModal();

			})
		}


	}
}
