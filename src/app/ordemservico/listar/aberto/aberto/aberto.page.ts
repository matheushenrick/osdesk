import { Component, OnInit } from '@angular/core';
import { OrdermservicoService } from 'src/app/ordemservico/ordermservico.service';
import { FilterPipe } from 'ngx-filter-pipe';
import { LoadingController, AlertController } from '@ionic/angular';
import { AplicacaoService } from 'src/app/service/aplicacao/aplicacao.service';

@Component({
	selector: 'app-aberto',
	templateUrl: './aberto.page.html',
	styleUrls: ['./aberto.page.scss'],
})
export class AbertoPage implements OnInit {
	
	/**
	 * Variavel de auxilio para a barra de pesquisa
	 * Utilizada em conjunto com o pipe filtro
	 * Deu trabalha mas funciona muito bem obrigado;
	 */
	textoBusca : any = {id:''};
	constructor(private os: OrdermservicoService, private filterPipe: FilterPipe, private loadingCtrl : LoadingController, private alertCtrl : AlertController, private app : AplicacaoService) { 
	}
	public oService: any;
	public analista: any

	async ngOnInit() {
		await this.getOpen();
		await this.preencheAnalista();
	}

	async getOpen() {
		let loading = await this.loadingCtrl.create({
			message : 'Carregando Ordens de Serviços Abertas',
			spinner : 'dots',
			mode : 'ios'
		})
		loading.present();

		return await this.os.geOsOpen().then((response) => {
			this.oService = response;
		}).finally(() => {loading.dismiss()});
	}

	filter(value){
		this.textoBusca = { $or: [{ id: value }, { str_servico_desc: value }, { dat_inclusao: value }, { str_descricao: value }, { str_problema_desc: value }] }
		this.filterPipe.transform(this.oService, this.textoBusca);
	}

	async preencheAnalista(){
		return await this.os.getAnalista().then((response) =>{
			console.log(response);
			this.analista = response;
		})
	}

	async encaminhar(os){
		let aleEnca = await this.alertCtrl.create({
			mode : 'ios',
			header: 'Encaminhar e Aceitar OS',
			subHeader : 'Encaminhe e Aceite esta Ordem de Serviço',
			message :  'Escolha o responsável por esta Ordem de Serviço',
			
			inputs : this.analista,

			buttons: [{
				text : 'Cancelar',
				role : 'cancel',
				cssClass : 'secondary'
			},{
				text : 'Encaminhar',
				cssClass : 'success',
				handler : async data =>{
					this.os.encaminhaOS(data, os);
					
				}
			}]
		})
		aleEnca.present();
		aleEnca.onDidDismiss().finally(() =>{
			this.getOpen();
		})

	}

	/**
	 * Deslizar para baixo para carregar os produtos.
	 * @param event evento
	 */
	async atualizar(event: any) {
		try {
			if (this.getOpen())
				event.target.complete();
		} catch (error) {
			console.log(error);
		}
	}
}
