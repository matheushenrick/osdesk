import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AndamentoPage } from './andamento.page';
import { FilterPipeModule } from 'ngx-filter-pipe';

const routes: Routes = [
  {
    path: '',
    component: AndamentoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FilterPipeModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AndamentoPage]
})
export class AndamentoPageModule {}
