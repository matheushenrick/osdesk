import { Component, OnInit } from '@angular/core';
import { OrdermservicoService } from 'src/app/ordemservico/ordermservico.service';
import { FilterPipe } from 'ngx-filter-pipe';
import { LoadingController, ModalController } from '@ionic/angular';
import { AcompanharPage } from 'src/app/ordemservico/acompanhar/acompanhar/acompanhar.page';

@Component({
	selector: 'app-andamento',
	templateUrl: './andamento.page.html',
	styleUrls: ['./andamento.page.scss'],
})
export class AndamentoPage implements OnInit {

	  /**
	   * Variavel de auxilio para a barra de pesquisa
	   * Utilizada em conjunto com o pipe filtro
	   * Deu trabalha mas funciona muito bem obrigado;
	   */
	textoBusca: any = { id: '' };
	constructor(private os: OrdermservicoService, private filterPipe: FilterPipe, private loadingCtrl: LoadingController, private modalCtrl : ModalController) {}
	public oService: any;

	async ngOnInit() {
		await this.getOSRun();
	}

	async getOSRun() {
		let loading = await this.loadingCtrl.create({
			message: 'Carregando Ordens de Serviços em Andamento',
			spinner: 'dots',
			mode: 'ios'
		})
		loading.present();

		return await this.os.getOSRun().then((response) => {
			this.oService = response;
		}).finally(() => { loading.dismiss() });
	}

	filter(value) {
		this.textoBusca = { $or: [{ id: value }, { str_servico_desc: value }, { dat_inclusao: value }, { str_descricao: value }, { str_problema_desc: value }] }
		this.filterPipe.transform(this.oService, this.textoBusca);
	}

	async acompanharAtendimento(os, acao){
		let acompanhar = await this.modalCtrl.create({
			component:AcompanharPage,
			componentProps : {dados_os : os, acao : acao}
		})

		acompanhar.present();
		acompanhar.onDidDismiss().then(() => this.getOSRun());
	}

	/**
	 * Deslizar para baixo para carregar os produtos.
	 * @param event evento
	 */
	async atualizar(event: any) {
		try {
			if (this.getOSRun())
				event.target.complete();
		} catch (error) {
			console.log(error);
		}
	}

}
