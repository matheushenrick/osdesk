import { Component, OnInit } from '@angular/core';
import { OrdermservicoService } from 'src/app/ordemservico/ordermservico.service';
import { FilterPipe } from 'ngx-filter-pipe';
import { LoadingController, AlertController } from '@ionic/angular';
import { AplicacaoService } from 'src/app/service/aplicacao/aplicacao.service';

@Component({
	selector: 'app-aceitas',
	templateUrl: './aceitas.page.html',
	styleUrls: ['./aceitas.page.scss'],
})
export class AceitasPage implements OnInit {

	/**
	   * Variavel de auxilio para a barra de pesquisa
	   * Utilizada em conjunto com o pipe filtro
	   * Deu trabalha mas funciona muito bem obrigado;
	   */
	textoBusca: any = { id: '' };
	constructor(private os: OrdermservicoService, private filterPipe: FilterPipe, private loadingCtrl: LoadingController, private alertCtrl: AlertController) {
	}
	public oService: any;
	public analista: any

	async ngOnInit() {
		await this.getOpen();
	}

	async getOpen() {
		let loading = await this.loadingCtrl.create({
			message: 'Carregando Ordens de Serviços Aceitas',
			spinner: 'dots',
			mode: 'ios'
		})
		loading.present();

		return await this.os.getOSPendind().then((response) => {
			console.log(response);
			this.oService = response;
		}).finally(() => { loading.dismiss() });
	}

	filter(value) {
		this.textoBusca = { $or: [{ id: value }, { str_servico_desc: value }, { dat_inclusao: value }, { str_descricao: value }, { str_problema_desc: value }] }
		this.filterPipe.transform(this.oService, this.textoBusca);
	}

	async iniciarAtendimento(os, index){
		console.log(index);
		let aleEnca = await this.alertCtrl.create({
			mode : 'ios',
			header: 'Iniciar Atendimento?',
			subHeader : 'Inicio de Atendimento da Ordem de Serviço',
			message :  'Deseja Iniciar o Atendimento desta Ordem de Serviço?',
			
			inputs : this.analista,

			buttons: [{
				text : 'Cancelar',
				role : 'cancel',
				cssClass : 'secondary'
			},{
				text : 'Iniciar',
				cssClass : 'success',
				handler : async () =>{
					this.os.iniciarAtendimento(os);
				}
			}]
		})
		aleEnca.present();
		aleEnca.onDidDismiss().finally(() =>{
			this.getOpen();
		})

	}

	/**
	 * Deslizar para baixo para carregar os produtos.
	 * @param event evento
	 */
	async atualizar(event: any) {
		try {
			if (this.getOpen())
				event.target.complete();
		} catch (error) {
			console.log(error);
		}
	}

}
