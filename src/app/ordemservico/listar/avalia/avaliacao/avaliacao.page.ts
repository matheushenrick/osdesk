import { Component, OnInit } from '@angular/core';
import { OrdermservicoService } from 'src/app/ordemservico/ordermservico.service';
import { FilterPipe } from 'ngx-filter-pipe';
import { LoadingController, ModalController } from '@ionic/angular';
import { AcompanharPage } from 'src/app/ordemservico/acompanhar/acompanhar/acompanhar.page';

@Component({
	selector: 'app-avaliacao',
	templateUrl: './avaliacao.page.html',
	styleUrls: ['./avaliacao.page.scss'],
})
export class AvaliacaoPage implements OnInit {

	/**
		  * Variavel de auxilio para a barra de pesquisa
		  * Utilizada em conjunto com o pipe filtro
		  * Deu trabalha mas funciona muito bem obrigado;
		  */
	textoBusca: any = { id: '' };
	constructor(private os: OrdermservicoService, private filterPipe: FilterPipe, private loadingCtrl: LoadingController, private modalCtrl : ModalController) {
	}
	public oService: any;

	async ngOnInit() {
		await this.getOSAvaliacao();
	}

	async getOSAvaliacao() {
		let loading = await this.loadingCtrl.create({
			message: 'Carregando Ordens de Serviços Pendentes de Avaliação',
			spinner: 'dots',
			mode: 'ios'
		})
		loading.present();

		return await this.os.getOSAvaliacao().then((response) => {
			this.oService = response;
			console.log(response);
		}).finally(() => { loading.dismiss() });
	}

	filter(value) {
		this.textoBusca = { $or: [{ id: value }, { str_servico_desc: value }, { dat_inclusao: value }, { str_descricao: value }, { str_problema_desc: value }] }
		this.filterPipe.transform(this.oService, this.textoBusca);
	}

	async avaliarAtendimento(os, acao){
		let acompanhar = await this.modalCtrl.create({
			component:AcompanharPage,
			componentProps : {dados_os : os, acao : acao}
		})

		acompanhar.present();
		acompanhar.onDidDismiss().then(() => this.getOSAvaliacao());
	}

	/**
	 * Deslizar para baixo para carregar os produtos.
	 * @param event evento
	 */
	async atualizar(event: any) {
		try {
			if (this.getOSAvaliacao())
				event.target.complete();
		} catch (error) {
			console.log(error);
		}
	}
}
