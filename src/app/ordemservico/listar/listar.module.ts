import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListarPage } from './listar.page';

const routes: Routes = [
	{
		path: 'os_listar',
		component: ListarPage,
		children: [{
			path: 'aberto',
			loadChildren : './aberto/aberto/aberto.module#AbertoPageModule'
		},{
			path: 'aceitas',
			loadChildren : './aceita/aceitas/aceitas.module#AceitasPageModule'
		},{
			path: 'andamento',
			loadChildren : './andamento/andamento/andamento.module#AndamentoPageModule'
		},{
			path: 'encerrado',
			loadChildren : './encerrado/encerrado/encerrado.module#EncerradoPageModule'
		},{
			path: 'avalia',
			loadChildren : './avalia/avaliacao/avaliacao.module#AvaliacaoPageModule'
		}]
	}, {
		path: '',
		redirectTo: 'os_listar/aberto',
		pathMatch : 'full'
	}
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes)
	],
	declarations: [ListarPage]
})
export class ListarPageModule { }
