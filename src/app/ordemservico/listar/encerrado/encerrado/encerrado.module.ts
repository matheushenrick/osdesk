import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EncerradoPage } from './encerrado.page';
import { FilterPipeModule } from 'ngx-filter-pipe';

const routes: Routes = [
  {
    path: '',
    component: EncerradoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FilterPipeModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EncerradoPage]
})
export class EncerradoPageModule {}
