import { Component, OnInit } from '@angular/core';
import { OrdermservicoService } from 'src/app/ordemservico/ordermservico.service';
import { FilterPipe } from 'ngx-filter-pipe';
import { LoadingController } from '@ionic/angular';

@Component({
	selector: 'app-encerrado',
	templateUrl: './encerrado.page.html',
	styleUrls: ['./encerrado.page.scss'],
})
export class EncerradoPage implements OnInit {

	/**
	   * Variavel de auxilio para a barra de pesquisa
	   * Utilizada em conjunto com o pipe filtro
	   * Deu trabalha mas funciona muito bem obrigado;
	   */
	textoBusca: any = { id: '' };
	constructor(private os: OrdermservicoService, private filterPipe: FilterPipe, private loadingCtrl: LoadingController) {
	}
	public oService: any;

	async ngOnInit() {
		await this.getOSClose();
	}

	async getOSClose() {
		let loading = await this.loadingCtrl.create({
			message: 'Carregando Ordens de Serviços Encerradas',
			spinner: 'dots',
			mode: 'ios'
		})
		loading.present();

		return await this.os.getOSClose().then((response) => {
			this.oService = response;
		}).finally(() => { loading.dismiss() });
	}

	filter(value) {
		this.textoBusca = { $or: [{ id: value }, { str_servico_desc: value }, { dat_inclusao: value }, { str_descricao: value }, { str_problema_desc: value }] }
		this.filterPipe.transform(this.oService, this.textoBusca);
	}

	/**
	 * Deslizar para baixo para carregar os produtos.
	 * @param event evento
	 */
	async atualizar(event: any) {
		try {
			if (this.getOSClose())
				event.target.complete();
		} catch (error) {
			console.log(error);
		}
	}
}
