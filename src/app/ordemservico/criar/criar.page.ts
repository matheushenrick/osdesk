import { Component, OnInit } from '@angular/core';
import { ConectaService } from 'src/app/service/conecta/conecta.service';
import { AplicacaoService } from 'src/app/service/aplicacao/aplicacao.service';
import { AlertController, LoadingController } from '@ionic/angular';

@Component({
	selector: 'app-criar',
	templateUrl: './criar.page.html',
	styleUrls: ['./criar.page.scss'],
})
export class CriarPage implements OnInit {

	private controller = 'osdesk008';

	constructor(private conecta: ConectaService, private app: AplicacaoService, private alert: AlertController, private loading: LoadingController) { }

	public envio: any = { str_equipe: '', str_servico: '', str_problema: '', str_prioridade: '', str_descricao: '' };
	public dicionario = { str_equipe: 'Equipe', str_servico: 'Serviço', str_problema: 'Problema', str_prioridade: 'Prioridade', str_descricao: 'Descrição' };
	public equipes: any;
	public prioridades: any;
	public servicos: any = [];
	public problemas: any = [];

	ngOnInit() {
		this.getEquipes();
	}

	async salvaDados() {

		if (this.app.empty(this.envio)) {
			this.app.infoAlert('Campos Obrigatórios', 'Campo ' + this.app.empty(this.envio) + ' é obrigatório e não foi preenchidos!');
		} else {

			let loading = await this.loading.create({
				message: 'Enviando dados para abertura da Ordem de Serviço',
				mode: 'ios',
				spinner: 'bubbles'
			})

			loading.present();

			let dados = await this.conecta.inclui(this.controller, this.envio).then((response) => {
				if (response['erro'] == 1) {
					this.app.infoAlert('Erro de Inserção', response['retorno']);
				}
				else {
					this.app.infoToast('Incluido com Sucesso', 'success');
					this.app.limpaObj(this.envio);
					this.app.navegar('home');

				}
			}).catch((response) => {
				this.conecta.errorHttp(response)

			}).finally(() => loading.dismiss());
			return dados;
		}
	}

	async getEquipes() {

		let loading = await this.loading.create({
			message: 'Carregando Prioridades',
			spinner: "circles"
		})
		loading.present();

		let retornaEquipe = await this.conecta.get('osdesk005', 'retornaEquipe').then(async (response) => {
			this.equipes = response;
			loading.dismiss();

			if (this.equipes.length == 0) {
				let alert = await this.alert.create({
					header: 'Cadastro de Serviços',
					message: 'Nenhuma equipe cadastrada, por favor cadastre para continuar',
					buttons: [{
						text: 'Cancelar',
						role: 'cancel',
						cssClass: 'secondary',
						handler: () => {
							this.app.navegar('home');
						}
					}, {
						text: 'Cadastrar Equipe',
						handler: () => {
							this.app.navegar('list_equipe');
						}
					}]
				})
				alert.present();
			}

		}).catch((response) => {
			loading.dismiss();
			this.conecta.errorHttp(response)
		});
		return retornaEquipe;
	}

	async getPrioridades() {

		let loading = await this.loading.create({
			message: 'Carregando Prioridades',
			spinner: "circles"
		})
		loading.present();

		let retornaPrioridades = await this.conecta.get('osdesk005', 'retornaPrioridades').then(async (response) => {
			this.prioridades = response;
			loading.dismiss();

		}).catch((response) => {
			this.conecta.errorHttp(response)

		});
		return retornaPrioridades;
	}

	async getServicos() {

		let loading = await this.loading.create({
			message: 'Carregando Serviços',
			spinner: "circles"
		})
		loading.present();

		let retornaServicos = await this.conecta.get('osdesk005', 'retornaServicos', 'equipe=' + this.envio.str_equipe).then(async (response) => {
			this.servicos = response;
			loading.dismiss();
		}).catch((response) => {
			loading.dismiss();
			this.conecta.errorHttp(response)
		});
		return retornaServicos;
	}

	async getProblemas() {

		let loading = await this.loading.create({
			message: 'Carregando Problema',
			spinner: "circles"
		})

		loading.present();

		let retornaProblemas = await this.conecta.get('osdesk005', 'retornaProblemas', 'servico=' + this.envio.str_servico + '&equipe=' + this.envio.str_equipe).then(async (response) => {
			this.problemas = response;
			loading.dismiss();
		}).catch((response) => {
			loading.dismiss();
			this.conecta.errorHttp(response)
		});
		return retornaProblemas;
	}
}
